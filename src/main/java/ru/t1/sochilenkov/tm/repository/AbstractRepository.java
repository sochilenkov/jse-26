package ru.t1.sochilenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.IRepository;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.model.AbstractModel;

import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    private final Map<String, M> models = new LinkedHashMap<>();

    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return new ArrayList<>(models.values());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(models.values());
        result.sort(comparator);
        return result;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        models.put(model.getId(), model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return models.get(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull final List<M> result = new ArrayList<>(models.values());
        return result.get(index);
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        models.remove(model.getId());
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        collection.stream()
                .map(AbstractModel::getId)
                .forEach(models::remove);
    }

}
