package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.IProjectTaskService;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.TaskIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.UserIdEmptyException;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public Task bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}
