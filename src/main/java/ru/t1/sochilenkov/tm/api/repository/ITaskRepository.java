package ru.t1.sochilenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
