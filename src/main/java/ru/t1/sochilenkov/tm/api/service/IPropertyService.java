package ru.t1.sochilenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    String getApplicationLog();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

}
